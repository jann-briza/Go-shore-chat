FROM golang:latest
COPY ./api .
CMD cd ./api
CMD go get github.com/gorilla/websockets
CMD go run main.go
EXPOSE 8000